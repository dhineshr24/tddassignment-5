package com.perfectnumber;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;
import static org.mockito.ArgumentMatchers.anyString;


public class ExampleTests{
   @Mock
    read file;
    number s;
    number s1;

    @Spy
    read file1;

    @Before
    public void setup() throws Exception{
      System.out.println("program started for this method");
      file = mock(read.class);
      s= new number();
      s.setreadobject(file);
    }
    @After
     public void setup1(){
      System.out.println("successfully executed the method");
    }
    @Test
    public void mainfunction() throws Exception{
      number nu = new number();
      nu.setreadobject(new read());
      if(nu.isPerfectNumber())
        System.out.println("Yes");
      else
        System.out.println("No");
    }
    @Test
   public void changeonaccess() throws Exception{
      when(file.reader(anyString())).thenReturn(100);
      boolean answer= s.isPerfectNumber();
      verify(file).reader(anyString());
      System.out.println(verify(file).reader(anyString()));
    }

    @Test
  public void isperfectnumber() throws Exception{
      when(file.reader(anyString())).thenReturn(100);
      boolean answer = s.isPerfectNumber();
      assertTrue(answer);

    }
    @Test
  public void invalidnumber() throws Exception{
      s1= new number();
      file1 = spy(new read());
      s1.setreadobject(file1);
      doReturn(10).when(file1).reader(anyString());
      boolean answer= number.isPerfectNumber();
      assertFalse(answer);
    }

}
